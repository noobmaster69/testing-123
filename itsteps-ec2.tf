resource "aws_security_group" "itsteps-ec2-sg" {
  name = "itsteps-ec2-sg"
  description = "Allow only SSH from specific IP and deny all both incoming and outgoing"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.external_public_restrict}"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["${var.external_public_restrict}"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["${var.external_public_allow}"]
  }

  vpc_id="${aws_vpc.itsteps-vpc.id}"

  tags {
    Name = "IT Step Academy EC2 SG"
  }
}

# Define EC2 inside the public subnet
resource "aws_instance" "itsteps-ec2" {
   ami  = "${var.ami}"
   instance_type = "t1.micro"
   subnet_id = "${aws_subnet.itsteps-public-subnet-aza.id}"
   vpc_security_group_ids = ["${aws_security_group.itsteps-ec2-sg.id}"]
   associate_public_ip_address = true
   disable_api_termination = false
   source_dest_check = false
   root_block_device {
       volume_size = 10
   }
   tags {
      Name = "IT Steps Academy EC2"
    }
}

# Define eip for bastionserver
resource "aws_eip" "ec2-eip" {
  instance = "${aws_instance.itsteps-ec2.id}"
  vpc = true
}