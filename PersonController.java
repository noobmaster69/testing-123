package StaffManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PersonController {

    @Autowired
    StaffR persons;

    @Autowired
    RoleR roles;

    @Autowired
    PaymentR payments;

    public Optional<Role> findRole(Person person){
        Role reqRole = person.getRole();
        Optional<Role> role = Optional.empty();

        if (reqRole != null) {
            if (reqRole.getId() != null) role = roles.findById(reqRole.getId());
            if (!role.isPresent()) role = roles.findByName(reqRole.getName());
        }

        return role;
    }

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public List<Person> getPerson(){

        return persons.findAll();
    }

    @RequestMapping(value = "/persons", method = RequestMethod.POST)
    public Person setPerson(@RequestBody Person person){
        Person person1 = new Person();
        Optional<Role> role = findRole(person);
        if(person.getName() != null && !person.getName().isEmpty()) {
            person1.setName(person.getName());
            person1.setSalary(person.getSalary());
            person1.setDoj(person.getDoj());
            person1.setId(person.getId());
            if (role.isPresent()) {
                person1.setRole(role.get());
            }
            person1.setNickName(person.getNickName());
            persons.save(person1);

            return person1;
        }

        throw new IllegalArgumentException("NOT FOUND");
    }

    @RequestMapping(value = "/persons/{personId}", method = RequestMethod.DELETE)
    public String deletePerson(@PathVariable("personId") String id){
        Optional<Person> person = persons.findById(id);
        List<Payment> payment = payments.findBypersonId(id);
        if(person.isPresent()){
            Person per = person.get();
            persons.delete(per);
            payments.deleteAll(payment);
            return "Deleted";
        }

        throw new IllegalArgumentException("Person not found.");
    }

    @RequestMapping(value = "/persons/{personId}", method = RequestMethod.PUT)
    public Person putPerson(@PathVariable("personId") String id, @RequestBody Person per){
        Optional<Person> person1 = persons.findById(id);
        Optional<Role> role = findRole(per);
        if(person1.isPresent()){
            Person person = person1.get();
            if(person.getName() != null && !person.getName().isEmpty()) {
                person.setName(per.getName());
                person.setSalary(per.getSalary());
                person.setDoj(per.getDoj());
                if(role.isPresent()){
                    person.setRole(role.get());
                }
                person.setNickName(per.getNickName());
                persons.save(person);

                return person;
            }
        }

        throw new IllegalArgumentException("Person not found.");
    }
}
