variable "aws_access_key" {
  description = "AWS access key"
  default = ""
}

variable "aws_secret_key" {
  description = "AWS secret key"
  default = ""
}

variable "aws_region" {
  description = "Region for the VPC"
  default = "ap-southeast-1"
}

variable "itsteps_vpc_cidr" {
  description = "CIDR for the ITSteps VPC"
  default = "10.13.0.0/16"
}

variable "itsteps_public_subnet_aza_cidr" {
  description = "CIDR for the public ITStep subnet Available Zone A"
  default = "10.13.100.0/24"
}

# ami-0dad20bd1b9c8c004: ubuntu server 18.04
# ami-0892a6b5c81270c10: ubuntu server 18.04 install common server template
variable "ami" {
  description = "Amazon Linux AMI"
  default = "ami-0892a6b5c81270c10"
}

variable "ssh_key" {
  description = "SSH Public Key path"
  default = "~/.ssh/id_rsa.pub"
}

variable "sshprivate_key" {
  description = "SSH Private Key path"
  default = "~/.ssh/id_rsa"
}

variable "remote_user" {
  description = "Remote users to EC2 instance"
  default = "se-deploy"
}

variable "dbusername" {
  description = "Postgres Username"
  default = "root"
}

variable "db_port" {
  description = "RDS Postgres Port"
  default = "5432"
}

# 96.9.77.116/32: TRYPE-Net
# 202.53.146.154/32: Seatel
# 96.9.79.213/32: ArrowDot

variable "external_public_restrict" {
  type = "list"
  description = "A list of restrict external ip to allow ingress rules"
  default = ["96.9.77.116/32","202.53.146.154/32", "110.74.193.154/32"]
}

variable "external_public_allow" {
  type = "list"
  description = "Allow all public network to access webserver"
  default = ["0.0.0.0/0"]
}
