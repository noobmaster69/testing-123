  package StaffManagement;

import org.springframework.data.annotation.Id;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Person {
    @Id
    private String id;
    private String name;
    private String nickName;
    private float salary;
    private Role role;

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    private float total;
    private final static DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    private String doj = df.format(new Date());

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public Person() {

    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total += total;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public float getSalary() {
        return salary;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDoj(String doj) {
        this.doj = doj;
    }

    public String getDoj() {
        return doj;
    }

    public void addTotal(float total){
        this.total += total;
    }

    public void minusTotal(float total){
        this.total -= total;
    }

}
