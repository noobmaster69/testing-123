package StaffManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class RoleController {

    @Autowired
    RoleR roles;

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    public List<Role> getRole(){

        return roles.findAll();
    }

    @RequestMapping(value = "/roles/{id}", method = RequestMethod.DELETE)
    public String deleteRole(@PathVariable("id") String id){
        Optional<Role> role = roles.findById(id);
        if(role.isPresent()){
            Role ro = role.get();
            roles.delete(ro);
            return "Deleted";
        }

        throw new IllegalArgumentException("Payment not found.");
    }

    @RequestMapping(value = "/roles/{rolesId}/{roleName}", method = RequestMethod.PUT)
    public Role putRole(@PathVariable("rolesId") String id, @PathVariable("roleName") String roleName){
        Optional<Role> role = roles.findById(id);
        if(role.isPresent()){
            Role ro = role.get();
            ro.setName(roleName);
            roles.save(ro);

            return ro;
        }

        throw new IllegalArgumentException("NOT FOUND");
    }
}
