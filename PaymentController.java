package StaffManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PaymentController {

    @Autowired
    PaymentR payments;

    @Autowired
    StaffR persons;

    @RequestMapping(value = "/payments", method = RequestMethod.GET)
    public List<Payment> getPayments(){

        return payments.findAll();
    }

    @RequestMapping(value = "/payments/{id}", method = RequestMethod.DELETE)
    public String deletePayment(@PathVariable("id") String id){
        Optional<Payment> payment = payments.findById(id);
        if(payment.isPresent()){
            Payment pa = payment.get();
            payments.delete(pa);
            return "Deleted";
        }

        throw new IllegalArgumentException("Payment not found.");
    }

    @RequestMapping(value = "persons/{personId}/payments", method = RequestMethod.POST)
    public Person setPayment(@RequestBody Payment pa, @PathVariable("personId") String id){
        Optional<Person> person = persons.findById(id);
        if(person.isPresent()){
            Person per = person.get();
            if(pa.getDetail().equals("-")) {
                per.minusTotal(pa.getAmount());
            }else{
                per.addTotal(pa.getAmount());
            }
            pa.setPersonId(per.getId());
            persons.save(per);
            payments.save(pa);
            return per;
        }

        throw new IllegalArgumentException("NOT FOUND");
    }

    //Searching Payment

    @RequestMapping(value = "/persons/{personId}/payments", method = RequestMethod.GET)
    public List<Payment> getPersonPayemntById(@PathVariable("personId") String id){
        Optional<Person> person = persons.findById(id);
        if(person.isPresent()){
            return  payments.findBypersonId(id);
        }else{
            payments.deleteBypersonId(id);
        }

        throw new IllegalArgumentException("PERSON NOT FOUND");
    }

    @RequestMapping(value = "/persons/{personId}/payments/{detail}", method = RequestMethod.GET)
    public List<Payment> getPersonPayemntByDetail(@PathVariable("personId") String id, @PathVariable("detail") String detail){
        Optional<Person> person = persons.findById(id);
        if(person.isPresent()){
            return payments.findByPersonIdAndDetail(id, detail);
        }

        throw new IllegalArgumentException("NOT FOUND");
    }
}
