# Define our VPC
resource "aws_vpc" "itsteps-vpc" {
  cidr_block = "${var.itsteps_vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name = "IT Steps Academy VPC"
  }
}

# Define the internet gateway
resource "aws_internet_gateway" "itsteps-igw" {
  vpc_id = "${aws_vpc.itsteps-vpc.id}"

  tags {
    Name = "IT Step Academy IGW"
  }
}

#

# Define the public subnet in Available Zone A
resource "aws_subnet" "itsteps-public-subnet-aza" {
  vpc_id = "${aws_vpc.itsteps-vpc.id}"
  cidr_block = "${var.itsteps_public_subnet_aza_cidr}"
  availability_zone = "ap-southeast-1a"

  tags {
    Name = "IT-Step-Academy-SN-AZA"
  }
}

# Define the route table
resource "aws_route_table" "itsteps-rt" {
  vpc_id = "${aws_vpc.itsteps-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.itsteps-igw.id}"
  }

  tags {
    Name = " IT-Step-Academy-RT"
  }
}

# Assign the route table associated to the public Subnet in AZ-B
resource "aws_route_table_association" "public-azb-rt" {
  subnet_id = "${aws_subnet.itsteps-public-subnet-aza.id}"
  route_table_id = "${aws_route_table.itsteps-rt.id}"
}
