package StaffManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class Payment {
    @Id
    private String id;
    private String personId;
    private float amount;
    private float total = 0;
    private String detail;
    private final static DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    private String realDate = df.format(new Date());

    public Payment() {

    }

    public Payment(float amount, String detail) {
        this.amount = amount;
        this.detail = detail;
    }

    public float getTotal() {
        return total;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public String getDetail() {
        return detail;
    }

    public float getAmount() {
        return amount;
    }

    public void setRealDate(String realDate) {
        this.realDate = realDate;
    }

    public String getRealDate() {
        return realDate;
    }
}
