package StaffManagement;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface StaffR extends MongoRepository<Person, String> {
    Optional<Person> findById(String id);
}
