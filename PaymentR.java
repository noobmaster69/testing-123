package StaffManagement;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;

@RepositoryRestResource(exported = false)
public interface PaymentR extends MongoRepository<Payment, String> {
    List<Payment> findBypersonId(String id);
    List<Payment> findByPersonIdAndDetail(String id, String detail);
    List<Payment> deleteBypersonId(String id);


}
